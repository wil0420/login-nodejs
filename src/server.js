var express = require('express');
var bodyParser = require('body-parser');
var passport = require('passport');
var mongoose = require('mongoose');
var config = require('./config/config');
var cors = require('cors')

var port = process.env.PORT || 5000;


var app = express();
app.use(cors());

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get('/',function(req,res){
    return res.send('Hola esta es la api '+ port + '/api');
})

app.use(passport.initialize())

var passportMiddleware = require('./middleware/passport');
passport.use(passportMiddleware)

var routes = require('./routes.js');

app.use('/api',routes);

mongoose.connect(config.db, {useNewUrlParser: true, useCreateIndex:true});

const connection = mongoose.connection;

connection.once('open',()=>{
    console.log('Se conecto a la base de datos correctamente');
})

connection.on('error',(err)=>{
    console.log("ha ocurrido un error en la conexion" + err)
    process.exit();
});


app.listen(port)


console.log("corriendo" + port)